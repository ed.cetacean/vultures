from django.contrib import admin
from .models import Puesto, Departamento, Materiales
from .forms import PuestoForm, DepartamentoForm, MaterialesForm

@admin.register(Puesto)
class PuestoAdmin(admin.ModelAdmin):
    form = PuestoForm
    list_display = ('Codigo', 'PuestoNombre', 'Descripcion')

@admin.register(Departamento)
class DepartamentoAdmin(admin.ModelAdmin):
    form = DepartamentoForm
    list_display = ('Codigo', 'DepartamentoNombre', 'Descripcion', 'NumeroExtension')

@admin.register(Materiales)
class MaterialesAdmin(admin.ModelAdmin):
    form = MaterialesForm
    list_display = ('Codigo', 'MaterialNombre', 'Descripcion')
