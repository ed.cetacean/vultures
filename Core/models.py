from django.db import models

## PUESTO:
class Puesto(models.Model):
    Codigo = models.CharField(max_length=4, primary_key=True)
    PuestoNombre = models.CharField(max_length=80, unique=True)
    Descripcion = models.CharField(max_length=80, blank=True, null=True)

    def str(self):
        return self.PuestoNombre


## DEPARTAMENTO:
class Departamento(models.Model):
    Codigo = models.CharField(max_length=4, primary_key=True)
    DepartamentoNombre = models.CharField(max_length=80, unique=True)
    Descripcion = models.CharField(max_length=80)
    FechaCreacion = models.DateField(auto_now_add=True)
    NumeroExtension = models.IntegerField(unique=True, blank=False, null=False)

    def str(self):
        return self.DepartamentoNombre

## PUESTO-DEPARTAMENTO (M:N):
#class PuestoDepto(models.Model):
 #   Puesto = models.ForeignKey(Puesto, on_delete=models.CASCADE)
  #  Departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
#   def str(self):
 #       return self.Departamento

## MATERIALES:
class Materiales(models.Model):
    Codigo = models.CharField(max_length=4, primary_key=True)
    MaterialNombre = models.CharField(max_length=80, unique=True)
    Descripcion = models.CharField(max_length=80)

    def str(self):
        return self.MaterialNombre


# ## ESTADO-INSCRIPCION:
# class EstadoInscripcion(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     Estado = models.CharField(max_length=20)

# ## RESPUESTAS:
# class Respuestas(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     Descripcion = models.CharField(max_length=80)

# ## PREGUNTAS:
# class Preguntas(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     Descripcion = models.CharField(max_length=80)
#     Respuestas = models.ForeignKey(Respuestas, on_delete=models.CASCADE)

# ## EXAMEN:
# class Examen(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     Nombre = models.CharField(max_length=50)
#     Descripcion = models.CharField(max_length=80)
#     TiempoLimite = models.IntegerField()
#     FechaElaboracion = models.DateField()
#     Calificacion = models.DecimalField(max_digits=5, decimal_places=2)
#     Preguntas = models.ForeignKey(Preguntas, on_delete=models.CASCADE)
#     Respuestas = models.ForeignKey(Respuestas, on_delete=models.CASCADE)

# ## CURSO:
# class Curso(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     Nombre = models.CharField(max_length=50)
#     Descripcion = models.CharField(max_length=80)
#     HorasDuracion = models.IntegerField()
#     Materiales = models.ForeignKey(Materiales, on_delete=models.CASCADE)
#     Examen = models.ForeignKey(Examen, on_delete=models.CASCADE)

# ## CAPACITACION:
# class Capacitacion(models.Model):
#     Numero = models.AutoField(primary_key=True)
#     FechaInicio = models.DateField()
#     FechaFinal = models.DateField()
#     Departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
#     Curso = models.ForeignKey(Curso, on_delete=models.CASCADE)

# ## EMPLEADOS:
# class Empleado(models.Model):
#     Numero = models.AutoField(primary_key=True)
#     NombrePila = models.CharField(max_length=60)
#     ApPaterno = models.CharField(max_length=60, blank=True, null=True)
#     ApMaterno = models.CharField(max_length=60, blank=True, null=True)
#     FechaContrato = models.DateField()
#     NumTel = models.CharField(max_length=15)
#     Email = models.CharField(max_length=80)
#     Puesto = models.ForeignKey(Puesto, on_delete=models.CASCADE)
#     Departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)

# ## EXAMEN-EMPLEADOS (M:N):
# class ExamenEmpleado(models.Model):
#     Empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
#     Examen = models.ForeignKey(Examen, on_delete=models.CASCADE)

# ## CONSTANCIA:
# class Constancia(models.Model):
#     Codigo = models.CharField(max_length=5, primary_key=True)
#     FechaEmision = models.DateField()
#     Capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE)
#     Empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)

# ## INSCRIPCION:
# class Inscripcion(models.Model):
#     Numero = models.AutoField(primary_key=True)
#     FechaInscripcion = models.DateField()
#     Empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
#     Estado = models.ForeignKey(EstadoInscripcion, on_delete=models.CASCADE)
#     Capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE)

# ## TIPO-INSTRUCTOR:
# class TipoInstructor(models.Model):
#     Codigo = models.CharField(max_length=3, primary_key=True)
#     ExternoInterno = models.CharField(max_length=7)

# ## INSTRUCTOR:
# class Instructor(models.Model):
#     Numero = models.AutoField(primary_key=True)
#     NombrePila = models.CharField(max_length=60)
#     ApPaterno = models.CharField(max_length=60, blank=True, null=True)
#     ApMaterno = models.CharField(max_length=60, blank=True, null=True)
#     FechaContrato = models.DateField()
#     NumTel = models.CharField(max_length=15)
#     Email = models.CharField(max_length=80)
#     TipoContrato = models.ForeignKey(TipoInstructor, on_delete=models.CASCADE)

# ## CAPACITACION-INSTRUCTOR (M:N):
# class CapacitacionInstructor(models.Model):
#     Capacitacion = models.ForeignKey(Capacitacion, on_delete=models.CASCADE)
#     Instructor = models.ForeignKey(Instructor, on_delete=models.CASCADE)

