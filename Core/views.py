
from django.views import generic
from django.shortcuts import render
from django.urls import reverse_lazy

from Core.forms import DepartamentoForm, MaterialesForm, PuestoForm, UpdateDepartamentoForm, UpdateMaterialesForm, UpdatePuestoForm
from Core.models import Puesto, Departamento, Materiales

## PUESTOS: ----------------------------------------------------------------- ##

## CREATE:
class CreatePuesto(generic.CreateView):
    model = Puesto
    form_class = PuestoForm
    template_name = "Core/Puesto/CreatePuesto.html"
    success_url = reverse_lazy('Core:ListPuesto')

## LIST:
class ListPuesto(generic.View):
    template_name = "Core/Puesto/ListPuesto.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Puesto.objects.all()

        self.context = {
            "PuestoQ" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdatePuesto(generic.UpdateView):
    model = Puesto
    form_class = UpdatePuestoForm
    template_name = "Core/Puesto/UpdatePuesto.html"
    success_url = reverse_lazy('Core:ListPuesto')

## DELETE:
class DeletePuesto(generic.DeleteView):
    model = Puesto
    template_name = "Core/Puesto/DeletePuesto.html"
    success_url = reverse_lazy('Core:ListPuesto')

## DETAIL:
class DetailPuesto(generic.DetailView):
    template_name = "Core/Puesto/DetailPuesto.html"
    model = Puesto

## DEPARTAMENTOS: ----------------------------------------------------------- ##

## CREATE:
class CreateDepartamento(generic.CreateView):
    model = Departamento
    form_class = DepartamentoForm
    template_name = "Core/Departamento/CreateDepartamento.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## LIST:
class ListDepartamento(generic.View):
    template_name = "Core/Departamento/ListDepartamento.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Departamento.objects.all()

        self.context = {
            "DepartamentoQ" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateDepartamento(generic.UpdateView):
    model = Departamento
    form_class = UpdateDepartamentoForm
    template_name = "Core/Departamento/UpdateDepartamento.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## DELETE:
class DeleteDepartamento(generic.DeleteView):
    model = Departamento
    template_name = "Core/Departamento/DeleteDepartamento.html"
    success_url = reverse_lazy('Core:ListDepartamento')

## DETAIL:
class DetailDepartamento(generic.DetailView):
    template_name = "Core/Departamento/DetailDepartamento.html"
    model = Departamento

## MATERIALES: -------------------------------------------------------------- ##

## CREATE:
class CreateMateriales(generic.CreateView):
    model = Materiales
    form_class = MaterialesForm
    template_name = "Core/Materiales/CreateMateriales.html"
    success_url = reverse_lazy('Core:ListMateriales')

## LIST:
class ListMateriales(generic.View):
    template_name = "Core/Materiales/ListMateriales.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Materiales.objects.all()

        self.context = {
            "MaterialesQ" : queryset
        }
        return render(request, self.template_name, self.context)

## UPDATE:
class UpdateMateriales(generic.UpdateView):
    model = Materiales
    form_class = UpdateMaterialesForm
    template_name = "Core/Materiales/UpdateMateriales.html"
    success_url = reverse_lazy('Core:ListMateriales')

## DELETE:
class DeleteMateriales(generic.DeleteView):
    model = Materiales
    template_name = "Core/Materiales/DeleteMateriales.html"
    success_url = reverse_lazy('Core:ListMateriales')

## DETAIL:
class DetailMateriales(generic.DetailView):
    template_name = "Core/Materiales/DetailMateriales.html"
    model = Materiales