
from django.urls import path
from Core import views

app_name = "Core"
urlpatterns = [
    ## PUESTOS:
    path('list/puestos/', views.ListPuesto.as_view(), name="ListPuesto"),
    path('create/puestos/', views.CreatePuesto.as_view(), name="CreatePuesto"),
    path('detail/puestos/<str:pk>/', views.DetailPuesto.as_view(), name="DetailPuesto"),
    path('update/puestos/<str:pk>/', views.UpdatePuesto.as_view(), name="UpdatePuesto"),
    path('delete/puestos/<str:pk>/', views.DeletePuesto.as_view(), name="DeletePuesto"),

    ## DEPARTAMENTOS:
    path('list/departamentos/', views.ListDepartamento.as_view(), name="ListDepartamento"),
    path('create/departamentos/', views.CreateDepartamento.as_view(), name="CreateDepartamento"),
    path('detail/departamentos/<str:pk>/', views.DetailDepartamento.as_view(), name="DetailDepartamento"),
    path('update/departamentos/<str:pk>/', views.UpdateDepartamento.as_view(), name="UpdateDepartamento"),
    path('delete/departamentos/<str:pk>/', views.DeleteDepartamento.as_view(), name="DeleteDepartamento"),

    ## MATERIALES:
    path('list/materiales/', views.ListMateriales.as_view(), name="ListMateriales"),
    path('create/materiales/', views.CreateMateriales.as_view(), name="CreateMateriales"),
    path('detail/materiales/<str:pk>/', views.DetailMateriales.as_view(), name="DetailMateriales"),
    path('update/materiales/<str:pk>/', views.UpdateMateriales.as_view(), name="UpdateMateriales"),
    path('delete/materiales/<str:pk>/', views.DeleteMateriales.as_view(), name="DeleteMateriales"),
]
