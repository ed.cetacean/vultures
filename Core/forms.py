from django import forms
from .models import Puesto, Departamento, Materiales

## PUESTO: ------------------------------------------------------------------ ##

class PuestoForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = "__all__"

        widgets = {
            "Codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Código del puesto"}),
            "PuestoNombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del puesto"}),
            "Descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Descripción del puesto"}),
        }

class UpdatePuestoForm(forms.ModelForm):

    class Meta:
        model = Puesto
        fields = "__all__"

        widgets = {
            "Codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Código del puesto"}),
            "PuestoNombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del puesto"}),
            "Descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Descripción del puesto"}),
        }

## DEPARTAMENTO: ------------------------------------------------------------ ##
class DepartamentoForm(forms.ModelForm):
    NumeroExtension = forms.CharField(max_length=4, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de extensión'}))

    class Meta:
        model = Departamento
        fields = "__all__"
        exclude = [ "FechaCreacion" ]

        widgets = {
            "Codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Codigo"}),
            "DepartamentoNombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"}),
        }

class UpdateDepartamentoForm(forms.ModelForm):
    NumeroExtension = forms.CharField(max_length=4, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de extensión'}))

    class Meta:
        model = Departamento
        fields = "__all__"
        exclude = [ "FechaCreacion" ]

        widgets = {
            "Codigo" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre"}),
            "DepartamentoNombre" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"}),
            "Descripcion" : forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Descripción"}),
        }



## MATERIALES: -------------------------------------------------------------- ##

class MaterialesForm(forms.ModelForm):
    class Meta:
        model = Materiales
        fields = "__all__"

        widgets = {
        "Codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder":"Código del material."}),
        "MaterialNombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder":"Nombre del material."}),
        "Descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder":"Descripción del material."}),
        }


class UpdateMaterialesForm(forms.ModelForm):
    class Meta:
        model = Materiales
        fields = "__all__"

        widgets = {
        "Codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Código del material."}),
        "MaterialNombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del material."}),
        "Descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Descripción del material."}),
        }