
from django.views import generic
from django.shortcuts import render

## INDEX:
class Index(generic.View):
    template_name = "Home/Index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Desarrollador" : "Vultures",
            "Empresa" : "UTT: Empresa Ficticia"
        }
        return render(request, self.template_name, self.context)

## ABOUT:
class About(generic.View):
    template_name = "Home/About.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Desarrollador" : "Vultures",
            "Empresa" : "UTT: Empresa Ficticia"
        }
        return render(request, self.template_name, self.context)

## CONTACT:
class Contact(generic.View):
    template_name = "Home/Contact.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "Desarrollador" : "Vultures",
            "Empresa" : "UTT: Empresa Ficticia"
        }
        return render(request, self.template_name, self.context)