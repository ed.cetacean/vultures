
from django.urls import path
from Home import views

app_name = "Home"
urlpatterns =[
    path('', views.Index.as_view(), name="Index"),
    path('About/', views.About.as_view(), name="About"),
    path('Contact/', views.Contact.as_view(), name="Contact"),
]